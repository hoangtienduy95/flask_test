import peewee
from config.settings import DATABASE, HOST, USER, PASSWORD, PORT

mysql_db = peewee.MySQLDatabase(
    DATABASE,
    host=HOST,
    user=USER,
    password=PASSWORD,
    port=PORT,
)
