import peewee

from models.connection import mysql_db


class BaseModel(peewee.Model):
    class Meta:
        mysql_db = mysql_db
        db_table = ''


class User(BaseModel):
    id = peewee.AutoField()
    username = peewee.CharField(unique=True)
    password = peewee.CharField()

    class Meta:
        database = mysql_db
        db_table = 'user'


class Token(BaseModel):
    id = peewee.AutoField()
    secret_token = peewee.CharField()
    secret_time_live = peewee.DateTimeField()
    access_token = peewee.CharField()
    access_time_live = peewee.DateTimeField()
    created_at = peewee.DateTimeField()
    user_id = peewee.ForeignKeyField(User, backref='token')
    deleted_at = peewee.DateTimeField(null=True)

    class Meta:
        database = mysql_db
        db_table = 'token'


def create_tables():
    with mysql_db:
        mysql_db.create_tables([User, Token])


create_tables()
