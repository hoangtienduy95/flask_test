from datetime import datetime
from uuid import uuid4
from functools import wraps

from flask import request, jsonify

from exception.user import TokenInvalid
from models.models import Token
from config.settings import ACCESS_TOKEN_TIME_LIVE


def authenticate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        authorization = request.headers.get("Authorization", None)
        if not authorization:
            response = {"message": TokenInvalid.content}
            return response, TokenInvalid.status_code
        access_token = request.headers.get("Authorization").split(" ")[1]
        try:
            token = Token.get(
                access_token=access_token,
                deleted_at=None,
            )
            if token.secret_time_live < datetime.now():
                return jsonify({"message": "A valid token is invalid!"})
            if token.access_time_live < datetime.now():
                token.access_token = uuid4()
                token.access_time_live = datetime.now() + ACCESS_TOKEN_TIME_LIVE
                token.save()
                kwargs = {
                    "token": token.access_token
                }
        except Token.DoesNotExist:
            response = {"message": TokenInvalid.content}
            return response, TokenInvalid.status_code
        return func(*args, **kwargs)
    return wrapper
