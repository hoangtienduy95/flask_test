import logging
from datetime import timedelta

DATABASE = 'test'
HOST = '0.0.0.0'
USER = 'root'
PASSWORD = 'mysql'
PORT = 3307
SECRET_TOKEN_TIME_LIVE = timedelta(days=7)
ACCESS_TOKEN_TIME_LIVE = timedelta(hours=1)

# Log
LOG_LEVEL = logging.INFO
LOG_FILE_NAME = 'app.log'
LOG_FORMAT = '[%(asctime)s] %(levelname)s in %(module)s: %(message)s'
