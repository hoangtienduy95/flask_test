import unittest
from models.models import User, Token


class BaseTest(unittest.TestCase):
    def setUp(self):
        self.user = User(
            username="username",
            password="password",
        )
        self.user.save()

    def tearDown(self):
        q = Token.delete().where(Token.user_id == self.user.id)
        q.execute()
        user = User.get(username=self.user.username)
        user.delete_instance()
