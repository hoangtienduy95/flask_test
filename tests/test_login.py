import unittest
import json

from app import app
from exception.user import UserDoesNotExist
from tests.base import BaseTest


class TestUser(BaseTest):
    def test_login_success(self):
        data = {
            "username": self.user.username,
            "password": self.user.password,
        }
        url = "/login"
        response = app.test_client().post(
            url,
            json=data,
        )
        assert response.status_code == 200
        assert json.loads(response.data).get("username") == self.user.username

    def test_login_wrong_username(self):
        data = {
            "username": "test",
            "password": self.user.password,
        }
        url = "/login"
        response = app.test_client().post(
            url,
            json=data,
        )
        assert response.status_code == UserDoesNotExist.status_code
        assert json.loads(response.data).get("message") == UserDoesNotExist.content
