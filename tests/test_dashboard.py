import json
from datetime import datetime
from uuid import uuid4

from app import app
from config.settings import SECRET_TOKEN_TIME_LIVE, ACCESS_TOKEN_TIME_LIVE
from exception.user import TokenInvalid
from models.models import Token
from tests.base import BaseTest


class TestDashboard(BaseTest):

    def setUp(self):
        super().setUp()
        secret_token = uuid4()
        access_token = uuid4()
        self.token = Token.create(
            secret_token=secret_token,
            secret_time_live=datetime.now() + SECRET_TOKEN_TIME_LIVE,
            access_token=access_token,
            access_time_live=datetime.now() + ACCESS_TOKEN_TIME_LIVE,
            created_at=datetime.now(),
            user_id=self.user.id,
        )

    def test_dashboard_success(self):
        response = app.test_client().get('/dashboard', headers={
            "Authorization": "Bearer" + " " + str(self.token.access_token)
        })
        assert response.status_code == 200
        assert json.loads(response.data).get("message") == "Dashboard"

    def test_dashboard_not_token(self):
        response = app.test_client().get('/dashboard')
        assert response.status_code == TokenInvalid.status_code
        assert json.loads(response.data).get("message") == TokenInvalid.content

