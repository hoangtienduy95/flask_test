import logging

from flask import Flask

from config.settings import LOG_LEVEL, LOG_FILE_NAME, LOG_FORMAT

app = Flask(__name__)

# Log config
logger = logging.getLogger(__name__)
logging.basicConfig(level=LOG_LEVEL)
handler = logging.FileHandler(LOG_FILE_NAME)
handler.setFormatter(logging.Formatter(LOG_FORMAT))

logger.addHandler(handler)

from apis import api

app.run(host="localhost", port=5005)
