from uuid import uuid4
from datetime import datetime

from flask import jsonify, request
from peewee import IntegrityError

from app import app
from exception.user import UserDoesNotExist
from models.models import User, Token
from config.settings import SECRET_TOKEN_TIME_LIVE, ACCESS_TOKEN_TIME_LIVE
from decorator.authentication import authenticate


@app.route("/")
def hello_world():
    return "Hello, World!"


@app.route('/register', methods=["POST"])
def register():
    username = request.json['username']
    password = request.json['password']
    confirm_password = request.json['confirm_password']
    if password != confirm_password:
        return jsonify({"Message": "password and confirm password is not same"})

    user = User()
    user.username = username
    user.password = password
    try:
        user.save()
    except IntegrityError:
        return jsonify({"Message": "User is existed"})
    return jsonify({"Message": "User register successfully"})


@app.route('/login', methods=["POST"])
def login():
    username = request.json['username']
    password = request.json['password']

    try:
        user = User.get(
            username=username,
            password=password
        )
        secret_token = uuid4()
        access_token = uuid4()
        token = Token.create(
            secret_token=secret_token,
            secret_time_live=datetime.now() + SECRET_TOKEN_TIME_LIVE,
            access_token=access_token,
            access_time_live=datetime.now() + ACCESS_TOKEN_TIME_LIVE,
            created_at=datetime.now(),
            user_id=user.id,
        )
    except User.DoesNotExist:
        response = {"message": UserDoesNotExist.content}
        return response, UserDoesNotExist.status_code
    result = dict(
        username=user.username,
        token=token.access_token
    )
    return jsonify(result)


@app.route('/dashboard', methods=["GET"])
@authenticate
def dashboard(*args, **kwargs):
    token = kwargs.get("token", None)
    data = {"message": "Dashboard"}
    if token:
        data["token"] = token
    return jsonify(data)


@app.route('/logout', methods=["POST"])
def logout():
    access_token = request.headers.get("Authorization").split(" ")[1]
    token = Token.get(
        access_token=access_token,
    )
    token.deleted_at = datetime.now()
    token.save()
    return jsonify({"Message": "logout is success"})
